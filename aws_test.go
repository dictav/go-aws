package aws

import (
	"fmt"
	. "github.com/dictav/go-aws"
	"net/http"
	"net/url"
	"testing"
)

func TestSNS(t *testing.T) {
	values := &url.Values{}
	values.Set("Action", "ListPlatformApplications")
	values.Set("Version", "2010-03-31")
	Sign("POST", "sns.ap-northeast-1.amazonaws.com", "/", values, "AKIAIVWAZPOBHCERRQGA", "QhJTngkZJ3YpDR5elXRh9ew0Tt3RhRtRVOhgrHuE")
	fmt.Println(values.Get("Timestamp"))
	res, err := http.PostForm("https://sns.ap-northeast-1.amazonaws.com/", *values)
	if err != nil {
		t.Fatal(err)
	}
	if code := res.StatusCode; code != 200 {
		t.Fatalf("Status code (%d) is not 200", code)
	}
}
