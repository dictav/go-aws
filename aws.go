package aws

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"net/url"
	"time"
)

type Region string

const (
	USGovWest    = Region("us-gov-west-1")
	USEast1      = Region("us-east-1")
	USWest1      = Region("us-west-1")
	USWest2      = Region("us-west-2")
	EUWest1      = Region("eu-west-1")
	EUCentral1   = Region("eu-central-1")
	APSoutheast1 = Region("ap-southeast-1")
	APSoutheast2 = Region("ap-southeast-2")
	APNortheast1 = Region("ap-northeast-1")
	SAEast1      = Region("sa-east-1")
	CNNorth1     = Region("cn-north-1")
)

func Sign(method, host, path string, v *url.Values, accessKey, secretKey string) (err error) {
	v.Set("AWSAccessKeyId", accessKey)
	v.Set("SignatureVersion", "2")
	v.Set("SignatureMethod", "HmacSHA256")
	v.Set("Timestamp", time.Now().Format(time.RFC3339))

	payload := method + "\n" + host + "\n" + path + "\n" + v.Encode()
	hash := hmac.New(sha256.New, []byte(secretKey))
	if _, err = hash.Write([]byte(payload)); err != nil {
		return errors.New("make hash error")
	}

	data := hash.Sum(nil)
	signature := base64.StdEncoding.EncodeToString(data)
	v.Set("Signature", signature)

	return err
}
